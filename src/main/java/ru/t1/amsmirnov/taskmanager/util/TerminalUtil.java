package ru.t1.amsmirnov.taskmanager.util;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() throws AbstractException {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception exception) {
            throw new NumberIncorrectException(value, exception);
        }
    }

}
