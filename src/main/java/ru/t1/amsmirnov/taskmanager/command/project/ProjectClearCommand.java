package ru.t1.amsmirnov.taskmanager.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    public static final String NAME = "project-clear";
    public static final String DESCRIPTION = "Clear project list.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS LIST]");
        getProjectService().clear();
    }

}
