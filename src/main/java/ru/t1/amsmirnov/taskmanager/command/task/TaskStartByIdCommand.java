package ru.t1.amsmirnov.taskmanager.command.task;

import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-start-by-id";
    public static final String DESCRIPTION = "Start task by ID.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeStatusById(id, Status.IN_PROGRESS);
    }

}
