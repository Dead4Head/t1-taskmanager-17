package ru.t1.amsmirnov.taskmanager.api.repository;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    void add(Project project) throws AbstractException;

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    void clear();

    Project findOneById(String id) throws AbstractException;

    Project findOneByIndex(Integer index) throws AbstractException;

    void remove(Project project) throws AbstractException;

    Project removeById(String id) throws AbstractException;

    Project removeByIndex(Integer index) throws AbstractException;

    Integer getSize();

    Boolean existById(String projectId) throws AbstractException;

}
