package ru.t1.amsmirnov.taskmanager.api.repository;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    void add(Task task) throws AbstractException;

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAllByProjectId(String projectId) throws AbstractException;

    void clear();

    Task findOneById(String id) throws AbstractException;

    Task findOneByIndex(Integer index) throws AbstractException;

    void remove(Task task) throws AbstractException;

    Task removeById(String id) throws AbstractException;

    Task removeByIndex(Integer index) throws AbstractException;

    Integer getSize();

}
